# Segment Folding

A small tool to generate an interactive animation of the NP-hardness reduction presented in our paper on efficient segment folding. 

The paper can be found on arXiv under:
https://arxiv.org/abs/2012.11062